Feature: As a Customer of RS Components,
         I want to able to order online a product
         So that, the product is home delivered to specified address

  Background:
  Delivery End to End Journey with pre conditions as:
   *Address and Payment are already added before only.


  Scenario: Home Delivering a product with Stock-497-9584

    When HMP- To visit "https://uk.rs-online.com/web/"
    And LOP- Login using "DivyaSurya" and "surya@123"
    Then HMP- Verify the user is logged in
    When HMP- Search RS Stock No "497-9584" in the Search Text Box
    And HMP- Click on Search Button
    Then PDP- Verify the product is displayed
    When PDP- Add to trolley
    When BAS- Navigate to Basket Page
    Then BAS- Verify the product details
    When BAS- Click on Delivery Button
    And BAS- Select the next day delivery option
    And BAS- Click on Checkout Securely Button
    When DEL- Add full name as "Divya Jagannatha Rao"
    And DEL- Click on Continue to Payment Button
    When PAY- Enter the Cvv number as "219"
    And PAY- Click on Order Review Button
    When ORV- Verify the Delivery address details
    And ORV- Verify the invoice address details
    And ORV- Verify the product details
    Then HMP- Quit Driver
