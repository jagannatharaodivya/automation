Feature: As a Customer of RS Components,
  I want to able to order online a product
  So that, I can collect the product from specified address

  Background:
  Collection End to End Journey with pre conditions as:
  *Address and Payment are already added before only.

    Scenario:
      When HMP- To visit "https://uk.rs-online.com/web/"
      And LOP- Login using "DivyaSurya" and "surya@123"
      Then HMP- Verify the user is logged in
      When HMP- Search RS Stock No "497-9584" in the Search Text Box
      And HMP- Click on Search Button
      When PDP- Add to trolley
      When BAS- Navigate to Basket Page
      Then BAS- Verify the product details
      When BAS- Click on Branch Collection Button
      And BAS- Select "Nuneaton Trade Counter" as option
      And BAS- Click on Checkout Securely Button
      Then COL- Verify the collection address
      When COL- Click on Payment Button
      When PAY- Enter the Cvv number as "219"
      And PAY- Click on Order Review Button
