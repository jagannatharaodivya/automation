package stepdefinitions;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import utils.SeleniumWait;
import utils.TestContext;

import java.util.List;

public class ProductDisplayPageSteps extends BaseSteps {
    public ProductDisplayPageSteps(TestContext context) {
        super(context);
    }

    @Then("^PDP- Verify the product is displayed$")
    public void verifyProductIsDisplayed() throws InterruptedException {

        List<WebElement> elements = driver.findElements(By.xpath("//div[@id=\"fsrInvite\"]//button[@id=\"fsrFocusFirst\"]"));
        if (elements.size() > 0 && elements.get(0).isDisplayed()) {
            homePage.getClosePopup().click();
            Thread.sleep(5000);
        } else {
            SeleniumWait.waitUntilDisplayed(productDisplayPage.getSkuId(), 5);
            Assert.assertTrue(productDisplayPage.getSkuId().getText().contains("497-9584"));
        }
    }

    @When("^PDP- Add to trolley$")
    public void pdpAddToTrolley() throws InterruptedException {
        List<WebElement> elements = driver.findElements(By.xpath("//div[@id=\"fsrInvite\"]//button[@id=\"fsrFocusFirst\"]"));
        if (elements.size() > 0 && elements.get(0).isDisplayed()) {
            homePage.getClosePopup().click();
            Thread.sleep(5000);
        } else {
            Thread.sleep(5000);
            productDisplayPage.getAddToBasket().click();
        }
    }

}
