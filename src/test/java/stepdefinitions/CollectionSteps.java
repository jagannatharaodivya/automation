package stepdefinitions;

import cucumber.api.PendingException;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import utils.TestContext;

import java.util.List;

public class CollectionSteps extends BaseSteps {
    public CollectionSteps(TestContext context) {
        super(context);
    }

    @Then("^COL- Verify the collection address$")
    public void colVerifyTheCollectionAddress() throws InterruptedException {
        List<WebElement> elements = driver.findElements(By.xpath("//div[@id=\"fsrInvite\"]//button[@id=\"fsrFocusFirst\"]"));
        if (elements.size() > 0 && elements.get(0).isDisplayed()) {
            homePage.getClosePopup().click();
            Thread.sleep(5000);
        } else {

            Assert.assertTrue("Collection address is not same as option selected", collectionDetailsPage.getCollectionAddress().getText().contains("Nuenaton Trade Counter"));
        }
    }

    @When("^COL- Click on Payment Button$")
    public void colClickOnPaymentButton() throws InterruptedException {

        List<WebElement> elements = driver.findElements(By.xpath("//div[@id=\"fsrInvite\"]//button[@id=\"fsrFocusFirst\"]"));
        if (elements.size() > 0 && elements.get(0).isDisplayed()) {
            homePage.getClosePopup().click();
            Thread.sleep(5000);
        } else {
            collectionDetailsPage.getPaymentButton().click();
        }

    }
}


