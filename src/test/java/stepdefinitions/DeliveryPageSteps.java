package stepdefinitions;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import utils.TestContext;

import java.util.List;

public class DeliveryPageSteps extends BaseSteps {

    public DeliveryPageSteps(TestContext context) {
        super(context);
    }

    @When("^DEL- Add full name as \"([^\"]*)\"$")
    public void delAddFullNameAsAndClickOnDeliveryAddAddressButton(String fullName) throws InterruptedException {
        List<WebElement> elements = driver.findElements(By.xpath("//div[@id=\"fsrInvite\"]//button[@id=\"fsrFocusFirst\"]"));
        if (elements.size() > 0 && elements.get(0).isDisplayed()) {
            homePage.getClosePopup().click();
            Thread.sleep(5000);
        } else {

            deliveryDetailsPage.getFullName().sendKeys(fullName);
        }
    }


    @And("^DEL- Click on Continue to Payment Button$")
    public void delClickOnContinueToPaymentButton() throws InterruptedException {
        List<WebElement> elements = driver.findElements(By.xpath("//div[@id=\"fsrInvite\"]//button[@id=\"fsrFocusFirst\"]"));
        if (elements.size() > 0 && elements.get(0).isDisplayed()) {
            homePage.getClosePopup().click();
            Thread.sleep(5000);
        } else {

            deliveryDetailsPage.getContinueToPaymentButton().click();
        }
    }
}
