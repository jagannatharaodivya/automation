package stepdefinitions;

import org.openqa.selenium.WebDriver;
import pageobjects.*;
import pageobjects.Delivery.DeliveryDetailsPage;
import pageobjects.Delivery.PaymentPage;
import pageobjects.Delivery.ReviewPage;
import pageobjects.collection.CollectionDetailsPage;
import utils.TestContext;

public class BaseSteps {
    protected final TestContext testContext;
    protected final WebDriver driver;
    protected final LoginPage loginPage;
    protected final HomePage homePage;
    protected final ProductDisplayPage productDisplayPage;
    protected final CheckoutPage checkoutPage;
    protected final BasketPage basketPage;
    protected final DeliveryDetailsPage deliveryDetailsPage;
    protected final PaymentPage paymentPage;
    protected final ReviewPage reviewPage;
    protected final CollectionDetailsPage collectionDetailsPage;
    protected final BrowserSteps browserSteps;


    protected BaseSteps(TestContext context) {
        testContext = context;
        this.driver = testContext.getWebDriverManager().getDriver();
        loginPage = testContext.getPageObjectManager().getLoginPage();
        homePage = testContext.getPageObjectManager().getHomePage();
        productDisplayPage = testContext.getPageObjectManager().productDisplayPage();
        checkoutPage = testContext.getPageObjectManager().getCheckoutPage();
        basketPage = testContext.getPageObjectManager().getBasketPage();
        deliveryDetailsPage = testContext.getPageObjectManager().getDeliveryDetailsPage();
        paymentPage = testContext.getPageObjectManager().getPaymentPage();
        reviewPage = testContext.getPageObjectManager().getReviewPage();
        collectionDetailsPage = testContext.getPageObjectManager().getCollectionDetailsPage();
        browserSteps = new BrowserSteps(driver);
    }
}
