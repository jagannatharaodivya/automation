package stepdefinitions;

import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import utils.TestContext;

import java.util.List;

public class HomePageSteps extends BaseSteps {
    private String sku;

    public HomePageSteps(TestContext context) {
        super(context);
    }

    @Given("^HMP- To visit \"([^\"]*)\"$")
    public void visitWebSite(String Url) throws InterruptedException {
        List<WebElement> elements = driver.findElements(By.xpath("//div[@id=\"fsrInvite\"]//button[@id=\"fsrFocusFirst\"]"));
        if (elements.size() > 0 && elements.get(0).isDisplayed()) {
            homePage.getClosePopup().click();
            Thread.sleep(5000);
        } else {

            driver.get(Url);
        }
    }

    @Then("^HMP- Verify the user is logged in$")
    public void verifyTheUserIsLoggedIn() throws InterruptedException {
        List<WebElement> elements = driver.findElements(By.xpath("//div[@id=\"fsrInvite\"]//button[@id=\"fsrFocusFirst\"]"));
        if (elements.size() > 0 && elements.get(0).isDisplayed()) {
            homePage.getClosePopup().click();
            Thread.sleep(5000);
        } else {

            Assert.assertTrue("User is not logged in", homePage.getWelcomeUser().getText().contains("Divya"));
        }
    }

    @Then("^HMP- Verify the results for following Search Criteria$")
    public void verifyTheResults(DataTable dataTable) throws InterruptedException {
        List<WebElement> elements = driver.findElements(By.xpath("//div[@id=\"fsrInvite\"]//button[@id=\"fsrFocusFirst\"]"));
        if (elements.size() > 0 && elements.get(0).isDisplayed()) {
            homePage.getClosePopup().click();
            Thread.sleep(5000);
        } else {

            List<List<String>> data = dataTable.raw();
            for (int i = 0; i < dataTable.raw().size(); i++) {
                homePage.getSearchTextBox().sendKeys(data.get(i).get(0));
                homePage.getSearchButton().click();
                Assert.assertTrue(productDisplayPage.getProductDescription().get(0).getText().contains(data.get(i).get(0)));
                productDisplayPage.getOrderByLtoH().click();
                driver.navigate().back();
            }
        }
    }

    @When("^HMP- Search RS Stock No \"([^\"]*)\" in the Search Text Box$")
    public void searchProduct(String productId) throws InterruptedException {
        List<WebElement> elements = driver.findElements(By.xpath("//div[@id=\"fsrInvite\"]//button[@id=\"fsrFocusFirst\"]"));
        if (elements.size() > 0 && elements.get(0).isDisplayed()) {
            Thread.sleep(5000);
            homePage.getClosePopup().click();
            Thread.sleep(5000);
        } else {
            sku = productId;
            Actions actions = new Actions(driver);
            actions.moveToElement(homePage.getProdSearch());
            homePage.getProdSearch().clear();
            homePage.getProdSearch().click();
            actions.sendKeys(productId);
            actions.build().perform();
        }
    }

    @And("^HMP- Click on Search Button$")
    public void clickOnSearchButton() throws InterruptedException {
        List<WebElement> elements = driver.findElements(By.xpath("//div[@id=\"fsrInvite\"]//button[@id=\"fsrFocusFirst\"]"));
        if (elements.size() > 0 && elements.get(0).isDisplayed()) {
            homePage.getClosePopup().click();
            Thread.sleep(5000);
        } else {
            Actions actions = new Actions(driver);
            actions.moveToElement(homePage.getProdSearch());
            actions.click();
            actions.sendKeys(Keys.ENTER);
            actions.build().perform();
        }
    }

    @Then("^HMP- Quit Driver$")
    public void hmpQuitDriver() {
        driver.quit();
    }

}
