package stepdefinitions;

import org.openqa.selenium.*;

public class BrowserSteps {

    private final WebDriver driver;
    private String mainWindowHandle;

    public BrowserSteps(WebDriver driver) {
        this.driver = driver;
    }

    /**
     * Store the main window to switch back to later
     */
    public void storeMainWindowHandle() {
        mainWindowHandle = getWindowHandle();
    }

    private JavascriptExecutor javascriptExecutor() {
        return (JavascriptExecutor) driver;
    }

    private String getWindowHandle() {
        return driver.getWindowHandle();
    }

    private void switchTo(String windowHandle) {
        driver.switchTo().window(windowHandle);
        driver.manage().window().fullscreen();
        ((JavascriptExecutor) driver).executeScript("window.focus();");
        if (windowHandle.equals(mainWindowHandle)) {
            System.out.println("Switched to MAIN window");
        } else {
            System.out.println("Switched to NEW window");
        }
    }


}