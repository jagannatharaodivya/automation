package stepdefinitions;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import utils.TestContext;

import java.util.List;

public class OrderReviewSteps extends BaseSteps {

    public OrderReviewSteps(TestContext context) {
        super(context);
    }

    @And("^ORV- Verify the product details$")
    public void orvVerifyTheProductDetails() throws InterruptedException {
        List<WebElement> elements = driver.findElements(By.xpath("//div[@id=\"fsrInvite\"]//button[@id=\"fsrFocusFirst\"]"));
        if (elements.size() > 0 && elements.get(0).isDisplayed()) {
            homePage.getClosePopup().click();
            Thread.sleep(5000);
        } else {
            Assert.assertTrue("Sku is not present", reviewPage.getskuId().getText().contains("497-9584"));
        }
    }

    @When("^ORV- Verify the Delivery address details$")
    public void orvVerifyTheAddressDetails() throws InterruptedException {
        List<WebElement> elements = driver.findElements(By.xpath("//div[@id=\"fsrInvite\"]//button[@id=\"fsrFocusFirst\"]"));
        if (elements.size() > 0 && elements.get(0).isDisplayed()) {
            homePage.getClosePopup().click();
            Thread.sleep(5000);
        } else {
            Assert.assertTrue("Delivery Address is different", reviewPage.getDeliveryAddress().getText().contains("mk6 2lx"));
        }
    }


    @And("^ORV- Verify the invoice address details$")
    public void orvVerifyTheInvoiceAddressDetails() throws InterruptedException {
        List<WebElement> elements = driver.findElements(By.xpath("//div[@id=\"fsrInvite\"]//button[@id=\"fsrFocusFirst\"]"));
        if (elements.size() > 0 && elements.get(0).isDisplayed()) {
            homePage.getClosePopup().click();
            Thread.sleep(5000);
        } else {
            Assert.assertTrue("Invoice Address is different", reviewPage.getInvoiceAddress().getText().contains("mk6 2lx"));
        }
    }
}
