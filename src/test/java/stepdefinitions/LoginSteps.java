package stepdefinitions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pageobjects.LoginPage;
import utils.TestContext;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class LoginSteps extends BaseSteps {

    public LoginSteps(TestContext context) {
        super(context);
    }

    @Given("^LOP- Login using \"([^\"]*)\" and \"([^\"]*)\"$")
    public void lopLoginUsingAnd(String username, String password) throws InterruptedException {
        List<WebElement> elements = driver.findElements(By.xpath("//div[@id=\"fsrInvite\"]//button[@id=\"fsrFocusFirst\"]"));
        if (elements.size() > 0 && elements.get(0).isDisplayed()) {
            homePage.getClosePopup().click();
            Thread.sleep(5000);
        } else {
            homePage.getLogInLink().click();
            loginPage.getUserName().sendKeys(username);
            loginPage.getPassword().sendKeys(password);
            loginPage.getLoginSubmitButton().click();
        }
    }

    @When("^LOP- Enter Login credentials After Basket Page$")
    public void enterLogindetails() throws InterruptedException {
        List<WebElement> elements = driver.findElements(By.xpath("//div[@id=\"fsrInvite\"]//button[@id=\"fsrFocusFirst\"]"));
        if (elements.size() > 0 && elements.get(0).isDisplayed()) {
            homePage.getClosePopup().click();
            Thread.sleep(5000);
        } else {
            String mwh = driver.getWindowHandle();
            System.out.println("Main Window Hanle" + mwh);
            Set s = driver.getWindowHandles();
            for (int i = 0; i < s.size(); i++) {
                System.out.println(s.iterator().next());
            }
            Iterator ite = s.iterator();

            while (ite.hasNext()) {
                String popupHandle = ite.next().toString();

                if (!popupHandle.contains(mwh)) {
                    driver.switchTo().window(popupHandle);
                    //**here you can perform operation in pop-up window**


                    //After finished your operation in pop-up just select the main window again
                    driver.switchTo().window(mwh);
                }
            }
        }
    }
}