package stepdefinitions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.*;
import utils.SeleniumWait;
import utils.TestContext;
import java.util.List;


public class BasketSteps extends BaseSteps {

    public BasketSteps(TestContext context) {
        super(context);
    }

    @When("^BAS- Navigate to Basket Page$")
    public void basNavigateToBasketPage() throws InterruptedException {
        List<WebElement> elements = driver.findElements(By.xpath("//div[@id=\"fsrInvite\"]//button[@id=\"fsrFocusFirst\"]"));
        if (elements.size() > 0 && elements.get(0).isDisplayed()) {
            homePage.getClosePopup().click();
            Thread.sleep(5000);
        } else {

            productDisplayPage.getBasket().click();
        }
    }

    @Then("^BAS- Verify the product details$")
    public void basVerifyTheProductDetails() throws InterruptedException {
        List<WebElement> elements = driver.findElements(By.xpath("//div[@id=\"fsrInvite\"]//button[@id=\"fsrFocusFirst\"]"));
        if (elements.size() > 0 && elements.get(0).isDisplayed()) {
            homePage.getClosePopup().click();
            Thread.sleep(5000);
        } else {

            Assert.assertTrue(basketPage.getBasSKUID().getText().contains("497-9584"));
        }
    }

    @When("^BAS- Click on Delivery Button$")
    public void basClickOnDelivery() throws InterruptedException {
        List<WebElement> elements = driver.findElements(By.xpath("//div[@id=\"fsrInvite\"]//button[@id=\"fsrFocusFirst\"]"));
        if (elements.size() > 0 && elements.get(0).isDisplayed()) {
            homePage.getClosePopup().click();
            Thread.sleep(5000);
        } else {

            basketPage.getDeliveryButton().click();
        }
    }

    @When("^BAS- Select the next day delivery option$")
    public void basSelectTheNextDayDeliveryOption() throws InterruptedException {
        List<WebElement> elements = driver.findElements(By.xpath("//div[@id=\"fsrInvite\"]//button[@id=\"fsrFocusFirst\"]"));
        if (elements.size() > 0 && elements.get(0).isDisplayed()) {
            homePage.getClosePopup().click();
            Thread.sleep(5000);
        } else {

            basketPage.getNextDayDelivery().click();
        }
    }

    @When("^BAS- Click on Checkout Securely Button$")
    public void basClickOnCheckoutSecurelyButton() throws InterruptedException {
        List<WebElement> elements = driver.findElements(By.xpath("//div[@id=\"fsrInvite\"]//button[@id=\"fsrFocusFirst\"]"));
        if (elements.size() > 0 && elements.get(0).isDisplayed()) {
            homePage.getClosePopup().click();
            Thread.sleep(5000);
        } else {
            basketPage.getCheckoutBottom().click();
        }
    }

    @When("^BAS- Click on Branch Collection Button$")
    public void basClickOnCollection() throws InterruptedException {
        List<WebElement> elements = driver.findElements(By.xpath("//div[@id=\"fsrInvite\"]//button[@id=\"fsrFocusFirst\"]"));
        if (elements.size() > 0 && elements.get(0).isDisplayed()) {
            homePage.getClosePopup().click();
            Thread.sleep(5000);
        } else {

            basketPage.getBranchCollectionButton().click();
        }
    }

    @And("^BAS- Select \"([^\"]*)\" as option$")
    public void basSelectAsOption(String branch) throws InterruptedException {
        List<WebElement> elements = driver.findElements(By.xpath("//div[@id=\"fsrInvite\"]//button[@id=\"fsrFocusFirst\"]"));
        if (elements.size() > 0 && elements.get(0).isDisplayed()) {
            homePage.getClosePopup().click();
            Thread.sleep(10000);
        } else {
            SeleniumWait.waitUntilDisplayed(basketPage.getWatfordTradeCounter(), 5);
            basketPage.getBranchOption().selectByVisibleText(branch);
        }
    }


}