package stepdefinitions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import utils.TestContext;

import java.util.List;

public class PaymentSteps extends BaseSteps {
    public PaymentSteps(TestContext context) {
        super(context);
    }

    @When("^PAY- Enter the Cvv number as \"([^\"]*)\"$")
    public void payEnterTheCvvNumberAs(String cvv) throws InterruptedException {
        List<WebElement> elements = driver.findElements(By.xpath("//div[@id=\"fsrInvite\"]//button[@id=\"fsrFocusFirst\"]"));
        if (elements.size() > 0 && elements.get(0).isDisplayed()) {
            homePage.getClosePopup().click();
            Thread.sleep(5000);
        } else {
            paymentPage.getCvv().sendKeys(cvv);
        }
    }

    @And("^PAY- Click on Order Review Button$")
    public void payClickOnOrderReviewButton() throws InterruptedException {
        List<WebElement> elements = driver.findElements(By.xpath("//div[@id=\"fsrInvite\"]//button[@id=\"fsrFocusFirst\"]"));
        if (elements.size() > 0 && elements.get(0).isDisplayed()) {
            homePage.getClosePopup().click();
            Thread.sleep(5000);
        } else {
            paymentPage.getContinueOrderReviewButton().click();
        }
    }
}

