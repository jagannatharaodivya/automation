package utils;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;

import static java.lang.Thread.*;

public class SeleniumWait {

    public static void waitUntilDisplayed(WebElement element, int waitSeconds) {
        boolean isDisplayed = false;
        int wait = waitSeconds*1000;
        while (wait > 0)
        {
            try {
                sleep(250);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            wait-=250;
            try {
                if (element.isDisplayed()) {
                    isDisplayed = true;
                    break;
                }
            } catch (NoSuchElementException | StaleElementReferenceException ignore) {}
        }
        if (!isDisplayed) {
            throw new NoSuchElementException("Element not visible after wait: " + element.toString());
        }
        }
    }

