package utils;

import org.openqa.selenium.WebDriver;
import pageobjects.*;
import pageobjects.Delivery.DeliveryDetailsPage;
import pageobjects.Delivery.PaymentPage;
import pageobjects.Delivery.ReviewPage;
import pageobjects.collection.CollectionDetailsPage;

public class PageObjectManager {
    private final WebDriver driver;

    public PageObjectManager(WebDriver driver) {
        this.driver = driver;
    }
    private LoginPage loginPage;
    private HomePage homePage;
    private ProductDisplayPage productDisplayPage;
    private CheckoutPage checkoutPage;
    private BasketPage basketPage;
    private DeliveryDetailsPage deliveryDetailsPage;
    private PaymentPage paymentPage;
    private ReviewPage reviewPage;
    private CollectionDetailsPage collectionDetailsPage;


    public LoginPage getLoginPage()
    {
        return (loginPage == null) ? loginPage = new LoginPage(driver) : loginPage;
    }

    public HomePage getHomePage()
    {
        return (homePage == null) ? homePage = new HomePage(driver) : homePage;
    }

    public ProductDisplayPage productDisplayPage()
    {
        return (productDisplayPage == null) ? productDisplayPage = new ProductDisplayPage(driver) : productDisplayPage;
    }

    public BasketPage getBasketPage() {
        return(basketPage == null) ? basketPage = new BasketPage(driver) : basketPage;
    }

    public CheckoutPage getCheckoutPage() {
        return(checkoutPage == null) ? checkoutPage = new CheckoutPage(driver) : checkoutPage;
    }

    public DeliveryDetailsPage getDeliveryDetailsPage() {
        return(deliveryDetailsPage == null) ? deliveryDetailsPage = new DeliveryDetailsPage(driver) : deliveryDetailsPage;
    }
    public PaymentPage getPaymentPage() {
        return(paymentPage == null) ? paymentPage = new PaymentPage(driver) : paymentPage;
    }

    public ReviewPage getReviewPage() {
        return(reviewPage == null) ? reviewPage = new ReviewPage(driver) : reviewPage;
    }

    public CollectionDetailsPage getCollectionDetailsPage() {
        return(collectionDetailsPage == null) ? collectionDetailsPage = new CollectionDetailsPage(driver) : collectionDetailsPage;
    }

}
