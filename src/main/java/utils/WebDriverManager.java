package utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class WebDriverManager {
    private String driverType;
    private WebDriver driver;

    public WebDriver getDriver() {
        if (driver == null) driver = createDriver();
        return driver;
    }

    private WebDriver createDriver() {

        driver = createLocalDriver();
        return driver;
    }

    private WebDriver createLocalDriver() {
        //Need to set the driver type //
        driverType = "CHROME";
        String CHROME_DRIVER_EXTENSION;
        String oSName = System.getProperty("os.name");
        if (oSName.contains("Mac")) {
            CHROME_DRIVER_EXTENSION = "mac/chromedriver";
        } else
            CHROME_DRIVER_EXTENSION = "win/chromedriver.exe";
        if (driverType.contains("FIREFOX")) {
            driver = new FirefoxDriver();
        }
        if (driverType.contains("CHROME")) {
            String driverPath = System.getProperty("user.dir") + "/src/test/resources/drivers/" + CHROME_DRIVER_EXTENSION;
            System.setProperty("webdriver.chrome.driver", driverPath);
            driver = new ChromeDriver();
        }
        if (driverType.contains("INTERNETEXPLORER")) {
          driver = new InternetExplorerDriver();

        }

        if(oSName.contains("Mac"))
        {
            driver.manage().window().fullscreen();
        }
        else {
            driver.manage().window().maximize();
        }
        return driver;
    }
}

