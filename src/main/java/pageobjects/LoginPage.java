package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage {
    @FindBy(how = How.NAME, using = "username")
    private WebElement userName;
    @FindBy(how = How.NAME, using = "j_password")
    private WebElement password;
    @FindBy(how = How.NAME, using = "loginBtn")
    private WebElement loginSubmitButton;
    @FindBy(how = How.NAME, using = "//*[@id=\"loginForm:LoginWidgetAction_username_decorate:LoginWidgetAction_username\"]")
    private WebElement usernameAfterBas;
    @FindBy(how = How.NAME, using = "//*[@id=\"loginForm:LoginWidgetAction_password_decorate:LoginWidgetAction_password\"]")
    private WebElement passAfterBas;
    @FindBy(how = How.NAME, using = "//*[@id=\"loginForm:login\"]")
    private WebElement subAfterBas;
    private WebDriverWait wait;


    public LoginPage(WebDriver driver) {
        PageFactory.initElements(driver, this);

    }

    public WebElement getUserName() {
        return userName;
    }

    public WebElement getPassword() {
        return password;
    }

    public WebElement getLoginSubmitButton() {
        return loginSubmitButton;
    }

    public void enterUsernameAndPasword(String usrname, String pwd, WebDriver driver) {
        userName.sendKeys(usrname);
        password.sendKeys(pwd);
    }

    public void clickLoginCredentialsSubmit() {
        loginSubmitButton.click();
    }

    public WebElement getPassAfterBas() {
        return passAfterBas;
    }

    public WebElement getUsernameAfterBas() {
        return usernameAfterBas;
    }

    public WebElement getSubAfterBas() {
        return subAfterBas;
    }
}
