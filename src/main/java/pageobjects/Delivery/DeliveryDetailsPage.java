package pageobjects.Delivery;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import static org.openqa.selenium.support.How.XPATH;

public class DeliveryDetailsPage {

    @FindBy(how = How.XPATH, using = "//*[@id=\"deliveryCollectionForm:FAOContactDetailWidgetAction_forAttentionOfName_decorate:FAOContactDetailWidgetAction_forAttentionOfName\"]")
    private WebElement fullName;
    @FindBy(how = How.XPATH, using = "//div[@id='listAddressListId']//a[@class='cssButton tertiary grey']")
    private WebElement addDeliveryAddressButton;
    @FindBy(how = How.XPATH, using = "//*[@id=\"listAddressListId\"]/div[2]/table/tbody/tr/td[2]/div")
    private WebElement displayedDeliveryAddress;
    @FindBy(how = How.XPATH, using = "//div[@class='headerErrorWidgetContainer COM21']//a[@class='cssButton primary blue enabledBtn']")
    private WebElement continueToPaymentButton;

    @FindBy(how = How.XPATH, using = "//*[@id=\"deliveryCollectionForm:AddDeliveryAddressWidgetAction_companyNameOne_decorate:AddDeliveryAddressWidgetAction_companyNameOne\"]")
    private WebElement formFullName;

    @FindBy(how = How.XPATH, using = "//*[@id=\"deliveryCollectionForm:AddDeliveryAddressWidgetAction_addressLineOne_decorate:AddDeliveryAddressWidgetAction_addressLineOne\"]")
    private WebElement addressLine1;

    @FindBy(how = How.XPATH, using = "//*[@id=\"deliveryCollectionForm:AddDeliveryAddressWidgetAction_addressLineThree_decorate:AddDeliveryAddressWidgetAction_addressLineThree\"]")
    private WebElement town;

    @FindBy(how = How.XPATH, using = "//*[@id=\"deliveryCollectionForm:AddDeliveryAddressWidgetAction_postCode_decorate:AddDeliveryAddressWidgetAction_postCode\"]")
    private WebElement postCode;

    @FindBy(how = How.XPATH, using = " //*[@id=\"deliveryCollectionForm:j_idt806\"]/span")
    private WebElement saveButton;


    public DeliveryDetailsPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public WebElement getFullName() {
        return fullName;
    }

    public WebElement getAddDeliveryAddressButton() {
        return addDeliveryAddressButton;
    }

    public WebElement getDisplayedDeliveryAddress() {
        return displayedDeliveryAddress;
    }

    public WebElement getContinueToPaymentButton() {
        return continueToPaymentButton;
    }

    public WebElement getFormFullName() {
        return formFullName;
    }

    public WebElement getAddressLine1() {
        return addressLine1;
    }

    public WebElement getTown() {
        return town;
    }

    public WebElement getPostCode() {
        return postCode;
    }

    public WebElement getSaveButton() {
        return saveButton;
    }
}

