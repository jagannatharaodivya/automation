package pageobjects.Delivery;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class PaymentPage  extends DeliveryDetailsPage{
    public PaymentPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(how = How.XPATH, using = "//div[@id='paymentForm:paymentSection']//a[@class='cssButton tertiary grey']")
    private WebElement addPaymentType;
    @FindBy(how = How.XPATH, using = "//div[@id='paymentForm:addPaymentWidgetPanel_content']//table[@class='cardTypeTbl top5']//tr//td[1]//input[@value='101']")
    private WebElement visaCardType;
    @FindBy(how = How.XPATH, using = "//*[@id=\"paymentForm:AddUserPaymentWidgetAction_cardNumber_decorate\"]/span/input[2]")
    private WebElement cardNumPart1;
    @FindBy(how = How.XPATH, using = "//*[@id=\"paymentForm:AddUserPaymentWidgetAction_cardNumber_decorate\"]/span/input[3]")
    private WebElement cardNumPart2;
    @FindBy(how = How.XPATH, using ="//*[@id=\"paymentForm:AddUserPaymentWidgetAction_cardNumber_decorate\"]/span/input[4]")
    private WebElement cardNumPart3;
    @FindBy(how = How.XPATH, using ="//*[@id=\"paymentForm:AddUserPaymentWidgetAction_cardNumber_decorate\"]/span/input[5]")
    private WebElement cardNumPart4;
    @FindBy(how = How.XPATH, using="//*[@id=\"paymentForm:AddUserPaymentWidgetAction_cardHolder_decorate:AddUserPaymentWidgetAction_cardHolder\"]")
    private WebElement cardHolderName;
    @FindBy(how = How.XPATH, using="//*[@id=\"paymentForm:AddUserPaymentWidgetAction_expiryDate_decorate:AddUserPaymentWidgetAction_expiryDate\"]")
    private WebElement expiryDate;
    @FindBy(how = How.XPATH, using="//*[@id=\"paymentForm:AddUserPaymentWidgetAction_cardName_decorate:AddUserPaymentWidgetAction_cardName\"]")
    private WebElement cardName;
    @FindBy(how = How.XPATH, using = "//*[@id=\"paymentForm:j_idt1731\"]/span")
    private WebElement saveButton;
    @FindBy(how = How.XPATH, using ="//*[@id=\"paymentForm:AddInvoiceAddressWidgetAction_invoiceAddressSameAsDelivery\"]")
    private WebElement invoiceSameAsDeliveryTickBox;
    @FindBy(how = How.XPATH, using ="//div[@class='headerErrorWidgetContainer COM21']//span[@id=\"checkoutSecurelyBtn\"]")
    private WebElement continueOrderReviewButton;
    @FindBy(how = How.XPATH, using = "//div[@class='cvvDiv']//div[@class='helpInputDiv']//input")
    private WebElement cvv;

    public WebElement getAddPaymentType() {
        return addPaymentType;
    }

    public WebElement getVisaCardType() {
        return visaCardType;
    }

    public WebElement getCardNumPart1() {
        return cardNumPart1;
    }

    public WebElement getCardNumPart2() {
        return cardNumPart2;
    }

    public WebElement getCardNumPart3() {
        return cardNumPart3;
    }

    public WebElement getCardNumPart4() {
        return cardNumPart4;
    }

    public WebElement getCardHolderName() {
        return cardHolderName;
    }

    public WebElement getExpiryDate() {
        return expiryDate;
    }

    public WebElement getCardName() {
        return cardName;
    }

    public WebElement getSaveButton() {
        return saveButton;
    }

    public WebElement getInvoiceSameAsDeliveryTickBox() {
        return invoiceSameAsDeliveryTickBox;
    }

    public WebElement getContinueOrderReviewButton() {
        return continueOrderReviewButton;
    }

    public WebElement getCvv() {
        return cvv;
    }
}
