package pageobjects.Delivery;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class ReviewPage {
    @FindBy(how = How.XPATH, using = "//*[@id=\"orderReviewForm\"]/div[1]/div[8]/div[1]/div[3]/div[3]/div[2]")
    private WebElement deliveryAddress;
    @FindBy(how = How.XPATH, using = "//*[@id=\"orderReviewForm\"]/div[1]/div[8]/div[2]/div[3]/div[1]/div[3]/div")
    private WebElement invoiceAddress;
    @FindBy(how = How.XPATH, using = "//*[@id=\"orderReviewForm:CoreDisplayBasketWidgetActionPaginationRefreshEventId\"]/table/tbody/tr[4]/td[2]/div/div[1]/span[2]")
    private WebElement skuId;

    public ReviewPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public WebElement getDeliveryAddress() {
        return deliveryAddress;
    }

    public WebElement getInvoiceAddress() {
        return invoiceAddress;
    }

    public WebElement getskuId() {
        return skuId;
    }


}
