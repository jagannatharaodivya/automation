package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

public class BasketPage {
    @FindBy(how = How.XPATH, using = "//*[@id=\"deliveryAndCollectionSelector\"]/div[1]/button")
    private WebElement deliveryButton;
    @FindBy(how = How.XPATH, using = "//*[@id=\"deliveryOptionsTable\"]//input[@value=5]")
    private WebElement nextDayDelivery;
    @FindBy(how = How.XPATH, using = "//div[@class='green header']//span[@class='atpMessage']")
    private WebElement inStockMessage;
    @FindBy(how = How.XPATH, using = "//*[@id='shoppingBasketForm:WebBasketLineWidgetActionCARTDIVId']//td[@class='quantityTd']//input[@type='text']")
    private List<WebElement> qtyList;
    @FindBy(how = How.XPATH, using = "//div[@class='cartNavigationDiv']//span[@id=\"checkoutSecurelyAndPuchBtn\"]")
    private WebElement checkoutBottom;
    @FindBy(how = How.XPATH, using = "//tr[@class='dataRow  lineRow']//td[@class='descriptionTd']//span[@class='textTitle']")
    private WebElement basSKUID;
    @FindBy(how = How.XPATH, using = "//*[@id=\"js-branchCollectionBtn\"]")
    private WebElement branchCollectionButton;
    @FindBy(how = How.XPATH, using = "//select[@id='shoppingBasketForm:tradecounterDropDown']")
    private WebElement branchOptions;
    @FindBy(how = How.XPATH, using = "//*[@id=\"shoppingBasketForm:tradecounterDropDown\"]/option[14]")
    private WebElement tradeCounter;
    public BasketPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public WebElement getBasSKUID() {
        return basSKUID;
    }

    public WebElement getDeliveryButton() {
        return deliveryButton;
    }

    public WebElement getNextDayDelivery() {
        return nextDayDelivery;
    }

    public WebElement getCheckoutBottom() {
        return checkoutBottom;
    }

    public WebElement getBranchCollectionButton() {
        return branchCollectionButton;
    }

    public Select getBranchOption() {
        return new Select(branchOptions);
    }

    public WebElement getWatfordTradeCounter() {
        return tradeCounter;
    }
}
