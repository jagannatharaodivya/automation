package pageobjects.collection;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class CollectionDetailsPage {
    @FindBy(how = How.XPATH, using = "//*[@id=\"deliveryCollectionForm:CheckoutBranchDetailsWidgetActionDELIVERY_OR_COLLECTION\"]/div/div[1]/div/pre/span")
    private WebElement collectionAddress;
    @FindBy(how = How.XPATH, using = "//*[@id=\"paymentForm:TCSoldToAddressWidgetAction_companyNameOne_decorate:TCSoldToAddressWidgetAction_companyNameOne\"]")
    private WebElement collectionFullName;
    @FindBy(how = How.XPATH, using = "//*[@id=\"checkoutSecurelyBtn\"]")
    private WebElement paymentButton;
    @FindBy(how = How.XPATH, using = "//*[@id=\"shoppingBasketForm:tradecounterDropDown\"]/option[17]")
    private WebElement watfordTradeCounter;
    public CollectionDetailsPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public WebElement getCollectionAddress() {
        return collectionAddress;
    }

    public WebElement getCollectionFullName() {
        return collectionFullName;
    }

    public WebElement getWatfordTradeCounter() {
        return watfordTradeCounter;
    }

    public WebElement getPaymentButton() {
        return paymentButton;
    }
}


