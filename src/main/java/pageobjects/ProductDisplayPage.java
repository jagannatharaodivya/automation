package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class ProductDisplayPage {
    @FindBy(how = How.XPATH, using = "//div[@class='search-matches']//h1//span[@class='search-term']")
    private WebElement searchMatchText;
    @FindBy(how = How.XPATH, using = "//div[@class='product-grid-container']//div[@data-qa='product-tile']")
    private List<WebElement> productList;
    @FindBy(how = How.XPATH, using = "//div[@class='product-grid-container']//div[@data-qa='product-tile']//div[@data-qa='description']")
    private List<WebElement> productDescription;
    @FindBy(how = How.XPATH, using = "//div[@class='product-grid-container']//div[@data-qa='product-tile']//button[@data-qa='add-to-basket-btn']")
    private List<WebElement> addButtonList;
    @FindBy(how = How.XPATH, using = "//i[@class='icon-cart']")
    private WebElement basket;
    @FindBy(how = How.XPATH, using = "//span[contains(text(),'Level Sensors & Switches Accessories')]//span[1]")
    private WebElement levelSensors;
    @FindBy(how = How.XPATH, using = "//span[contains(text(),'Space Heaters & Radiators')]//span[1]")
    private WebElement radiators;
    @FindBy(how = How.XPATH, using = "//span[contains(text(),'Enclosure Heaters')]//span[1]")
    private WebElement heaters;
    @FindBy(how = How.XPATH, using = "//*[@id=\"search-results\"]/div[3]/div/div[2]/div[4]/div[2]/div/div/section[1]/label/div/button/span[1]")
    private WebElement orderByLtoH;
    @FindBy(how = How.XPATH, using = "//*[@id=\"pagecell\"]/div/div[1]/div[1]/div[3]/ul/li[1]/span[2]")
    private WebElement skuId;
    @FindBy(how = How.XPATH, using ="//div[@class='add-to-basket-container']//button[@class='btn btn-primary-red btn-large btn-add-to-basket']")
    private WebElement addToBasket;
    public ProductDisplayPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public WebElement getSearchMatchText() {
        return searchMatchText;
    }

    public List<WebElement> getProductList() {
        return productList;
    }

    public List<WebElement> getProductDescription() {
        return productDescription;
    }

    public List<WebElement> getAddButtonList() {
        return addButtonList;
    }

    public WebElement getBasket() {
        return basket;
    }

    public WebElement getLevelSensors() {
        return levelSensors;
    }

    public WebElement getRadiators() {
        return radiators;
    }

    public WebElement getHeaters() {
        return heaters;
    }

    public WebElement getOrderByLtoH() {
        return orderByLtoH;
    }

    public WebElement getSkuId() {
        return skuId;
    }

    public WebElement getAddToBasket() {
        return addToBasket;
    }
}


