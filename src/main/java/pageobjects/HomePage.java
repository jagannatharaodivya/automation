package pageobjects;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class HomePage {
    @FindBy(how = How.ID, using = "js-logInOut")
    private WebElement logInLink;
    @FindBy(how = How.ID, using = "js-welcome")
    private WebElement welcomeUser;
    @FindBy(how = How.ID, using = "js-logInOut")
    private WebElement logOut;
    @FindBy(how = How.ID, using = "searchTerm")
    private WebElement searchTextBox;
    @FindBy(how = How.ID, using ="btnSearch")
    private WebElement searchButton;
    @FindBy(how = How.ID, using ="//a[@id='js-logInOut']")
    private WebElement logOutLink;
    @FindBy(how = How.XPATH, using ="//*[@id=\"sidebar\"]/section/div[1]/div/ul/li[1]/a/div/span")
    private WebElement barCodeReader;
    @FindBy(how = How.XPATH, using ="//*[@id=\"sidebar\"]/section/div[1]/div/ul/li[2]/a/div/span")
    private WebElement capProxSensor;
    @FindBy(how = How.XPATH, using = "//*[@id=\"sidebar\"]/section/div[1]/div/ul/li[3]/a/div/span")
    private WebElement currentTransducers;
    @FindBy(how = How.XPATH, using = "//*[@id=\"searchTerm\"]")
    private WebElement prodSearch;
    @FindBy(how = How.XPATH, using = "//div[@id=\"fsrInvite\"]//button[@id=\"fsrFocusFirst\"]")
    private WebElement closePopup;

    public HomePage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public WebElement getLogInLink() {
        return logInLink;
    }

    public WebElement getWelcomeUser() {
        return welcomeUser;
    }

    public WebElement getLogOut() {
        return logOut;
    }

    public WebElement getSearchTextBox() {
        return searchTextBox;
    }

    public WebElement getSearchButton() {
        return searchButton;
    }

    public WebElement getLogOutLink() {
        return logOutLink;
    }

    public WebElement getBarCodeReader() {
        return barCodeReader;
    }

    public WebElement getCapProxSensor() {
        return capProxSensor;
    }

    public WebElement getCurrentTransducers() {
        return currentTransducers;
    }

    public void clickOnLoginLink()
    {
        logInLink.click();
    }

    public Boolean isUserIdDisplayed(WebDriver driver) {
        Boolean yN= welcomeUser.isDisplayed();
        return yN;
    }

    public WebElement getProdSearch() {
        return prodSearch;
    }

    public WebElement getClosePopup() {

       return closePopup;
    }
}
